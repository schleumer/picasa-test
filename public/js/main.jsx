var request = require('superbird');
var React = require('react');

var MaxImages = 50;

var Image = React.createClass({
    render: function() {
      console.log("Image::render");
      return <div className="gallery-image-holder">
        <div className="title">
          <a href={this.props.data.url} target="_blank">
            <b>{this.props.data.title}</b>
          </a>
        </div>
        <div className="image">
          <a href={this.props.data.image} target="_blank">
            <img src={this.props.data.thumbnail} className="gallery-image"/>
          </a>
        </div>
        <div className="title">
          <a href={this.props.data.album} target="_blank">
            <b>{this.props.data.author}</b>
          </a>
        </div>
      </div>;
    }
});


var ImageSet = React.createClass({
    render: function() {
      console.log("ImageSet::render");
      function buildElement(image) {
        return <Image key={image.id} data={image} />;
      };
      return <div>{this.props.images.map(buildElement)}</div>;
    }
});

var Galery = React.createClass({
    getInitialState: function() {
      console.log("galery::getInitialState");
      return {
          images: []
        , search: "paisagens"
        , index: 1
        , total: 0
        , current: 0
      };
    }
  , boilData: function() {
      console.log("galery::boilData");
      var self = this;
      request
        .get('/data')
        .query({'query': this.state.search, 'index': this.state.index})
        .set('Accept', 'application/json')
        .end()
        .then((res) => {
          this.setState({
            images: res.body.entries,
            total: res.body.total,
            current: res.body.current,
            real: res.body.real
          });
        });
    }
  , componentDidMount: function() {
      console.log("galery::componentDidMount");
      this.boilData();
    }
  , handleSubmit: function(e) {
      console.log("galery::handleSubmit");
      e.preventDefault();
      this.setState({
        index: 1
      }, this.boilData);
    }
  , handleChange: function(key) {
      return function(event) {
        console.log("galery::handleChange");
        // WORKAROUND ALERT
        var obj = {};
        obj[key] = event.target.value;
        this.setState(obj);
      }.bind(this);
    }
  , nextPage: function() {
      console.log("galery::nextPage");
      this.setState({
        index: this.state.index + MaxImages
      }, this.boilData);
    }
  , prevPage: function() {
      console.log("galery::prevPage");
      this.setState({
        index: this.state.index - MaxImages
      }, this.boilData);
    }
  , render: function() {
      console.log("galery::render");
      return <div>
        <form onSubmit={this.handleSubmit} className="col-xs-6">
          <div className="input-group">
            <input type="text" className="form-control" name="search" value={this.state.search} onChange={this.handleChange('search')} />
            <span className="input-group-btn">
              <button className="btn btn-default" type="submit">Go!</button>
            </span>
          </div>
        </form>
        <div className="btn-toolbar col-xs-6">
          <div className="btn-group pull-left">
            <button className="btn btn-default" onClick={this.prevPage}>prev</button>
          </div>
          <div className="title pull-left">
            <h4>{this.state.index} / {this.state.total} / {this.state.current}({this.state.images.length} / {this.state.real})</h4>
          </div>
          <div className="btn-group pull-left">
            <button className="btn btn-default" onClick={this.nextPage}>next</button>
          </div>
        </div>
        <ImageSet images={this.state.images} className="col-xs-12" />
      </div>;
    }
});

React.render(<Galery /> , document.getElementById('holder'));