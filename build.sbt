import spray.revolver.RevolverPlugin._

Revolver.settings

Revolver.reColors := Seq("green")

name := """picasa-crawler"""

version := "1.0"

scalaVersion := "2.11.5"

resolvers += "spray repo" at "http://repo.spray.io"

//resolvers += "spray nightlies" at "http://nightlies.spray.io"

resolvers += "sona" at "https://oss.sonatype.org/content/repositories/releases/"

resolvers += "akka" at "http://repo.akka.io/snapshots/"

resolvers += "akka releases" at "http://repo.akka.io/releases/"

libraryDependencies ++= Seq(
  "com.github.scala-incubator.io" %% "scala-io-core" % "0.4.3",
  "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.3",
  "com.chuusai" %% "shapeless" % "1.2.4",
  "com.typesafe.akka"             %% "akka-actor"       % "2.4-SNAPSHOT",
  "com.typesafe.akka"             %% "akka-slf4j"       % "2.4-SNAPSHOT",
  "io.spray"                      %% "spray-can"        % "1.3.2-20140909",
  "io.spray"                      %% "spray-routing"    % "1.3.2-20140909",
  "io.spray"                      %% "spray-json"       % "1.3.1"
)

scalacOptions ++= Seq(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Ywarn-dead-code",
  "-language:_",
  "-target:jvm-1.7",
  "-encoding", "UTF-8"
)

testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")

fork := true

fork in Test := true